Name:           perl-Curses-UI
Version:        0.9609
Release:        1
Summary:        Curses based OO user interface framework
License:        GPL+ or Artistic
URL:            https://metacpan.org/release/Curses-UI
Source0:        https://cpan.metacpan.org/authors/id/M/MD/MDXI/Curses-UI-%{version}.tar.gz
# Support Unicode, bug #2043798, CPAN RT#56695, proposed to the upstream
Patch0:         Curses-UI-0.9609-Use-wide-character-aware-functions.patch
BuildArch:      noarch
BuildRequires:  coreutils
BuildRequires:  findutils
BuildRequires:  make
BuildRequires:  perl-interpreter
BuildRequires:  perl-generators
BuildRequires:  perl(base)
BuildRequires:  perl(Carp)
BuildRequires:  perl(Curses) >= 1.29
BuildRequires:  perl(Cwd)
BuildRequires:  perl(DynaLoader)
BuildRequires:  perl(Exporter)
BuildRequires:  perl(File::Find)
BuildRequires:  perl(File::Spec)
BuildRequires:  perl(File::Temp)
BuildRequires:  perl(FileHandle)
BuildRequires:  perl(FindBin)
BuildRequires:  perl(inc::Module::Install)
BuildRequires:  perl(lib)
BuildRequires:  perl(Module::Install::AutoInstall)
BuildRequires:  perl(Module::Install::Metadata)
BuildRequires:  perl(Module::Install::WriteAll)
BuildRequires:  perl(strict)
BuildRequires:  perl(Term::ReadKey)
BuildRequires:  perl(Test)
BuildRequires:  perl(Test::More)
BuildRequires:  perl(Test::Pod)
BuildRequires:  perl(Test::Simple)
BuildRequires:  perl(vars)
BuildRequires:  perl(warnings)
Requires:       perl(:MODULE_COMPAT_%(eval "`%{__perl} -V:version`"; echo $version))

%description
Curses::UI is an object-oriented user interface framework for Perl.

%prep
%autosetup -p1 -n Curses-UI-%{version}
rm -r inc
sed -i -e '/^inc\// d' MANIFEST
find -type f -exec chmod -x {} +

%build
%{__perl} Makefile.PL INSTALLDIRS=vendor
make %{?_smp_mflags}

%install
make pure_install DESTDIR=$RPM_BUILD_ROOT

find $RPM_BUILD_ROOT -type f -name .packlist -exec rm -f {} \;

%{_fixperms} $RPM_BUILD_ROOT/*

%check
make test

%files
%doc Changes CREDITS README examples
%{perl_vendorlib}/*
%{_mandir}/man3/*

%changelog
* Thu Jul 07 2022 jchzhou <jchzhou@outlook.com> - 0.9609-1
- Init package from fedora
